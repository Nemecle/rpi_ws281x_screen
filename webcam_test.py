#!/usr/bin/python3

import cv2

cv2.namedWindow("preview")
vc = cv2.VideoCapture(0)

width = 10
height = 25
# vc.set(cv2.CAP_PROP_FRAME_WIDTH, width)
# vc.set(cv2.CAP_PROP_FRAME_HEIGHT, height)
dimensions = (width,height)

if vc.isOpened(): # try to get the first frame
    rect, frame = vc.read()
    scaled_frame = cv2.resize(frame, dimensions, interpolation =cv2.INTER_AREA)
else:
    rect = False

while rect:
    cv2.imshow("preview", scaled_frame)
    rect, frame = vc.read()
    scaled_frame = cv2.resize(frame, dimensions, interpolation =cv2.INTER_AREA)

    key = cv2.waitKey(20)
    if key == 27: # exit on ESC
        break

vc.release()
cv2.destroyWindow("preview")

