#!/usr/bin/env python3
# rpi_ws281x library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.

import time
from rpi_ws281x import *
import argparse
import random
import cv2
import signal
import sys

# LED strip configuration:
LED_COUNT      = 250 # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 200 #255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

REFRESH_RATE_MS = 10

SHOW_GUI = False

def signal_handler(sig, frame):
    print('end program on SIGINT.')
    reset()
    sys.exit(0)

# set by xy
def led_xy(x,y,color):
    if (x % 2):
        i = x * 25 + (25 - (y + 1))
    else:
        i = x * 25 + y
    strip.setPixelColor(i, color)

# reset
def reset():
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, Color(0,0,0))
    strip.show()


def cam_pixel_to_color(frame,x,y):
    """
    takes a cv2 frame, and return the pixel colour at (x,y) as Color
    while converting BGR (cam) to appropriate RGB

    While working RGB order might be convoluted, to fix

    """

    try:
        colour = Color(
                    # int(min (255, frame[x][y][1] * 1.5)), #B -> G
                    # int(frame[x][y][2]), #G -> R
                    # int(frame[x][y][0]) #R -> B
                    int(frame[y][x][1]), #B -> G
                    int(frame[y][x][2]), #G -> R
                    int(frame[y][x][0]) #R -> B
               )

        return colour
    except Exception as e:
        print(f"[WARNING] Out of Bound on camera for ({x}, {y})")
        return Color(0,0,0)

    return -1


if __name__ == '__main__':
    
    # Capture SIGINT to turn guirlande off on Ctrl-C

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    signal.signal(signal.SIGINT, signal_handler)

    reset()

    c = Color(20, 20, 20)


    if SHOW_GUI:
        cv2.namedWindow("preview")

    vc = cv2.VideoCapture(0)

    width = 10
    height = 25
    dimensions = (width,height)
    
    
    if vc.isOpened(): # try to get the first frame
        rect, frame = vc.read()
        rescaled_frame = cv2.flip(
                                 cv2.resize(frame, (width * 20, height * 20), interpolation =cv2.INTER_AREA), 1)
    else:
        rect = False
    
    # exit()

    while rect:
        if SHOW_GUI:
            cv2.imshow("preview", rescaled_frame)
        rect, frame = vc.read()
        scaled_frame = cv2.flip(cv2.resize(frame, dimensions, interpolation
        =cv2.INTER_AREA), 1)
        rescaled_frame = cv2.resize(scaled_frame, (width * 20, height * 20), interpolation =cv2.INTER_AREA)
    
        key = cv2.waitKey(20)
        if key == 27: # exit on ESC
            break

        for x in range(0, width):
            for y in range(0, height):
                led_xy(x,y, cam_pixel_to_color(scaled_frame, x, y))
        strip.show()
        time.sleep(REFRESH_RATE_MS/1000)
    
    vc.release()
    reset()
    if SHOW_GUI:
        cv2.destroyWindow("preview")

