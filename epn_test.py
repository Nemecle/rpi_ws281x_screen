#!/usr/bin/env python3
# rpi_ws281x library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.

import time
from rpi_ws281x import *
import argparse
import random
import cv2

# LED strip configuration:
LED_COUNT      = 250 # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53



# Define functions which animate LEDs in various ways.
def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms/1000.0)

def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

def rainbow(strip, wait_ms=20, iterations=1):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((i+j) & 255))
        strip.show()
        time.sleep(wait_ms/1000.0)

# Main program logic follows:
if __name__ == '__main__':

    # initialize webcam capture and preview
    cv2.namedWindow("preview")
    vc = cv2.VideoCapture(0)

    width = LED_COUNT
    height = int(LED_COUNT * (3/4))

    dimensions = (width, height)

    # verify capture status
    # if vc.isOpened():
    #     rect, frame = vc.read()
    #     scaled_frame = cv2.resize(frame, dimensions, interpolation
    #     =cv2.INTER_AREA)
    # else:
    #     rect = False
    #     print("[ERROR] Could not read webcam: maybe it is not plugged" 
    #     "(do you even have one?)")
    #     exit(1)

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    # while rect:
    #     cv2.imshow("preview", scaled_frame)
    #     rect, frame = vc.read()
    #     scaled_frame = cv2.resize(frame, dimensions, interpolation
    #     =cv2.INTER_AREA)

    #     key = cv2.waitKey(20)
    #     if key == 27: # exit on ESC
    #         break

    #     for i in range(strip.numPixels()):
    #         color = Color(
    #             int(min (255, scaled_frame[3][i][1] * 1.5)), #B -> G
    #             int(scaled_frame[3][i][2]), #G -> R
    #             int(scaled_frame[3][i][0])) #R -> B

    #         strip.setPixelColor(i, color);

    #     strip.show()

    #     print(scaled_frame[3][10]);
    #     time.sleep(200/1000)


    # vc.release()
    # cv2.destroyWindow("preview")


    while True:
        for i in range(strip.numPixels()):
            color = Color(int(random.randint(0,255)), int(random.randint(0,255)),int(random.randint(0,255)))
            strip.setPixelColor(i, color);
            strip.show()
            # time.sleep(50/1000)

    # while True:
    #     strip.setPixelColor(1, Color(0,0,0));
    #     strip.show();
    #     strip.setPixelColor(1, Color(255,255,255));
    #     strip.show();


    # colorWipe(strip, Color(0,0,0), 10)
